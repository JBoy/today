// Simple service to handle local storage data
angular.module('today.services', ['LocalStorageModule'])
.factory('TodayService', function($rootScope,localStorageService) {
    return {
        // Return Local Storage data
        all: function() {
            if(localStorageService.get('todays') === null){
                return [];
            }
            return localStorageService.get('todays').reverse();
        },
        // Clean out Local Storage
        clear: function(){
            localStorageService.clearAll();
        },
        // Rewrite Local Storage
        rewriteLocalStorage: function(todayObj){
            this.clear();
            localStorageService.add('todays', JSON.stringify(todayObj));
        },
        // Clear yesterdays 'todays'
        removeYesterday: function (todayObj) {
            if(todayObj.length && new Date(todayObj[0].doe).getUTCDate() < new Date().getUTCDate()){
                this.clear();
            }
        }
    };
});


