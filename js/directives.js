angular.module('today.directives', [])
.directive('ngEnter', function(){
    return function($scope, element, attributes){
        element.bind("keypress", function(event){
            if(event.which === 13){
                $scope.$apply(function(){
                    $scope.$eval(attributes.ngEnter);
                });
            }
        });
    }
});