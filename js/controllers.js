// Todays controller
angular.module('today.controllers', []).controller('todayController', function ($scope, TodayService) {

    // Clear old todays
    TodayService.removeYesterday(TodayService.all());

    // Retrieve todays
    $scope.todays = TodayService.all();
    $scope.newToday = '';


    $scope.$watch('todays', function (newValue, oldValue) {
        // Logic to toggle show/hide "remove done"
        angular.element(document).ready(function () {
            var nrOfChecked = document.querySelectorAll("input:checked").length;
            var removeDoneBtn = document.querySelector('.removeDone');
            nrOfChecked > 0 ? removeDoneBtn.removeAttribute('disabled') : removeDoneBtn.setAttribute('disabled', 'disabled');
        });

        if(newValue !== oldValue){
            TodayService.rewriteLocalStorage(newValue);
        }

    }, true);


    $scope.addToday = function () {
        if ($scope.newToday.title) {
            $scope.todays.push({id: this.getNewId(), title: $scope.newToday.title, done: false, doe: new Date});
            $scope.newToday = '';
            // rewrite Local Storage
            TodayService.rewriteLocalStorage($scope.todays);
        }
    };

    $scope.removeDone = function(){
        $scope.todays.forEach(function(t) {
            $scope.remove(t);
        });
    };

    $scope.remove = function () {

        var oldTodays = $scope.todays;
        TodayService.clear();
        $scope.todays = [];
        angular.forEach(oldTodays, function(today) {
            if (!today.done) $scope.todays.push(today);
        });
        TodayService.rewriteLocalStorage($scope.todays);
    };

    $scope.getNewId = function () {
        return $scope.todays.length + 1;
    };
});