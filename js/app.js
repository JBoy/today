// Today App


var Today = angular.module('today', [
    'ngRoute',
    'today.services',
    'today.controllers',
    'today.directives'
]);

Today.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when("/",
        {
            templateUrl: "partials/todays.html",
            controller: "todayController"
        }
    ).otherwise({
            redirectTo: '/'
    });
}]);